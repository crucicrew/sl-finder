# Running SL-Finder

SL-Finder is a tool for finding stem-loops in DNA that match a number of parameters. It was designed for use identifying replication-associated stem-loops in CRESS (Circular Rep-Encoding Single-Stranded) DNA viruses, but is not limited to this application. Currently, SL-Finder must be used on a Mac or Linux environment. It is a command line tool that is available as a Docker image.



How to install:

1. Download and install Docker
   - [Docker Desktop](https://www.docker.com/products/docker-desktop) (Mac or Linux)
   - [Docker Engine](https://docs.docker.com/engine/install/) (Linux)
2. Run Docker
3. Download the "sl-finder_script.sh" file from [this repository](https://bitbucket.org/crucicrew/sl-finder/src/master/) to a directory in your local environment
4. From the command line, navigate to the directory where you have downloaded the script file
5. Deactivate any virtual or conda environments (if active) with the command "deactivate"
6. Enter the command `chmod +x ./sl-finder_script.sh`



How to run:

On the command line where the script file is located, run, at a minimum, `./sl-finder_script.sh [fasta input file] [gff input file] [gff output file]`, with each bracketed item representing a relative or absolute path to the file. None of the files can have spaces in their names, and the input files and output file should be distinct.


Optional input flags (following the required input):

- `--motif`: Conserved motif within desired loops, such as nonanucleotide (ambiguous bases ok). Must be less than the ideal loop length. Overrides family selection. default=NANTANTAN
- `--family`: CRESS viral family (geminiviridae, genomoviridae, smacoviridae, cycloviridae, circoviridae, or general), used to find Stem-Loops from family's optimal motif
- `--idealstemlen` or `-s`: Stem length for found stems to be scored against, default=11
- `--ideallooplen` or `-l`: Loop length for found loops to be scored against, default =11
- `--frame` or`-f`: Number of nucleotides surrounding motif sequence used to predict secondary structure, default = 15

Example: `./sl-finder_script.sh ./fastain.fasta ./gffin.gff ./out.gff --family geminiviridae`

Notes: 

- Privileges: Since SL-Finder invokes Docker, running it requires root privileges or addition to the `docker` group on the machine. To create a `docker` group, a user with root privileges may run `sudo groupadd docker`, then `sudo usermod -aG docker $USER`, with `$USER` replaced by the desired username as described in the [Docker documentation](https://docs.docker.com/engine/install/linux-postinstall/).
- Output: The GFF file generated by SL-Finder contains the genome annotations for each of the predicted nonanucleotide sequences and stem-loops. When importing this GFF file into a genome browser (e.g. Geneious) containing a copy of the input sequences, the annotations can be previewed and merged to form a file containing the annotated genomes.
- Bug reporting: Users can report issues running SL-Finder via the "Issues" tab in the repository. 

Modifications coming soon:

- `--verbose` or `-v`: Standard output (all putative stem-loop sequences and dot-bracket secondary structures) will only be visible with this flag
- `--CSVout`: Creates a CSV file to write to