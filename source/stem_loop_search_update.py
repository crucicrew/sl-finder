# coding: utf-8
import RNA, argparse, gffutils
from tinyfasta import FastaParser, FastaRecord
from pos_stem_loop import PosStemLoop

iupac_to_nuc = {'a': 'a', 't': 'tu', 'g': 'g', 'c': 'c', 'r': 'ag', 'y': 'ctu', 's': 'gc', 'w': 'atu', 'k': 'gtu',
                'm': 'ac', 'b': 'cgtu', 'd': 'agtu', 'h': 'actu', 'v': 'acg', 'n': 'acgtu', 'u':'ut', }

def transform_func(x):
    # adds some text to the end of transcript IDs
    if 'transcript_id' in x.attributes:
        x.attributes['transcript_id'][0] += '_transcript'
    return x

parser = argparse.ArgumentParser(description='Look for CRESS stem-loops in GFF files')

parser.add_argument("FASTAin", help = "Filename to look in (FASTA)", type = argparse.FileType('r'))
parser.add_argument("GFFin", help = "Filename to look in (GFF)")
parser.add_argument("out", help = "GFF file to write to", action='store')
parser.add_argument("--motif", help="Conserved motif within desired loops, such as nonanucleotide (ambiguous bases ok)", default="nantantan")
parser.add_argument("--params", help="Parameter file to read (dna_mathews1999.par or dna_mathews2004.par)", default="dna_mathews2004.par")
parser.add_argument("--family", help = "CRESS viral family", type = str, choices=['geminiviridae', 'genomoviridae', 'smacoviridae', 'cycloviridae', 'circoviridae', 'general'], default = None)
parser.add_argument("--idealstemlen", "-s", help = "Stem length for found stems to be scored against", type = int, default=11)
parser.add_argument("--ideallooplen", "-l", help = "Loop length for found loops to be scored against", type = int, default =11)
parser.add_argument("--frame", "-f", help = "number of nucleotides around nonanucleotide sequence to fold at a time", type = int, default = 15)
parser.add_argument("--quadrunauto", "-a", help = "runs 4 simultaneous programs with the permutations of the two frames and DNA parameters identified", action = "store_true", default = False)
parser.add_argument("--params2", help = "second parameter file to read for quad run")
parser.add_argument("--frame2", help = "second frame for quad run", type = int, default = 20)
parser.add_argument("--twofamilies", help = "adds a second motif to quad run", action = "store_true", default = False)
parser.add_argument("--family2", help = "second family for quad run", type = str)
parser.add_argument("--CSVout",help = "CSV file to write to", action='store', default = None)
args = parser.parse_args()

genomes = FastaParser(args.FASTAin.name)



# Use specific MFE parameters, ex /Users/alyssa/src/ViennaRNA-2.4.9/misc/dna_mathews1999.par
RNA.read_parameter_file(args.params)


def reverse_complement(bases, isDNA):
    if(isDNA):
        complement = {'a': 't', 'c': 'g', 'g': 'c', 't': 'a', 'r': 'y', 'y': 'r', 'm': 'k', 'k': 'm', 'b': 'v', 'd': 'h', 'h': 'd', 'n': 'n', 'w':'w'}
    else:
        complement= {'a': 'u', 'c': 'g', 'g': 'c', 'u': 'a', 'r': 'y', 'y': 'r', 'm': 'k', 'k': 'm', 'b': 'v', 'd': 'h', 'h': 'd', 'n': 'n'}
    return ''.join([complement[base] for base in bases[::-1]])

def ambiguitiesToBases(ambiguousDNA):
    posBases = []
    for i in range(len(ambiguousDNA)):
        posBases.append(iupac_to_nuc[ambiguousDNA[i]])
    return posBases

def equivalentTo(ambiguousDNA, givenSeq):
    for i in range(len(givenSeq)):
        cur_char = givenSeq[i]
        if not cur_char in iupac_to_nuc[ambiguousDNA[i].lower()]:
            return False
    return True

def removeDuplicates(pos_loops):
    new_pos_loops = []
    if(len(pos_loops) > 0):
        new_pos_loops.append(pos_loops[0])
    for i in range(1, len(pos_loops)):
        if (pos_loops[i]!=pos_loops[i-1]):
            new_pos_loops.append(pos_loops[i])
    return new_pos_loops

def bestNona(type):
    if type == "geminiviridae":
        #best = "taatattac"
        best = "trakattrc"
    if type == "circoviridae":
        best = "tagtattac"
    if type ==  "cycloviridae":
        best = "taatattac"
    if type == "general":
        best = "tagtattac"
    if type == "genomoviridae":
        best = "tawwdhwan"
    if type ==  "smacoviridae":
        best = "nakwrttac"
    return best

def nonaSearch(sequence, name, type, basesAroundNona):
    if type == "geminiviridae":
        return motifSearch(sequence, name, "trakattrc", basesAroundNona)
    elif type == "cycloviridae" or type == "circoviridae":
        return motifSearch(sequence, name, "nantattac", basesAroundNona)
    elif type == "genomoviridae":
        return motifSearch(sequence, name, "tawwdhwan", basesAroundNona)
    elif type == "smacoviridae":
        return motifSearch(sequence, name, "nakwrttac", basesAroundNona)
    elif type == "general":
        general_nonas = ["nantantan", "nakwrttac", "tawwdhwan", "trakattrc"]
        stem_loops = [motifSearch(sequence, name, nona, basesAroundNona)
                      for nona in general_nonas]
        return [stem_loop for loops in stem_loops for stem_loop in loops]

def motifSearch(sequence, name, motif, basesAroundMotif):
    pos_motifs = []
    motif_len = len(motif)
    for i in range(basesAroundMotif, len(sequence) - (motif_len - 1)):
        subsequence = sequence[i:i + motif_len]
        expanded_start = i - basesAroundMotif
        expanded_end = i + basesAroundMotif + motif_len
        expanded_subsequence = sequence[expanded_start:expanded_end]
        if equivalentTo(motif, subsequence):
            pos_motifs.append(PosStemLoop(name, expanded_subsequence,
                                          expanded_start, subsequence, i + 1,
                                          False))
        elif equivalentTo(reverse_complement(motif, True), subsequence):
            pos_motifs.append(PosStemLoop(name, expanded_subsequence,
                              expanded_start,
                              reverse_complement(subsequence, True),
                              i + 1, True))
    return pos_motifs


fasta_records = []
features =[]
cur_features=[]

recording = False
with open(args.GFFin) as seqs:
    for line in seqs:
        line.rstrip()
        if not recording:
            if line.startswith('##sequence-region'):
                recording = True
        elif line.startswith('##FASTA'):
            recording = False
        elif not (line.startswith('##')):
            #print(line)
            cur_features.append(gffutils.feature.feature_from_line(line))


db = gffutils.create_db(cur_features, ":memory:", merge_strategy='create_unique', keep_order=True)
scores=[]
str_scores=[]
features2=[]

if args.quadrunauto == False:
    for genome in genomes:
        fasta_record = FastaRecord(str(genome.description) + ' sl')
        strSeq = (str(genome.sequence) + (str(genome.sequence)[0:50])).lower()
        if args.family is not None:
            pos_loops = nonaSearch(strSeq, genome.description, args.family, args.frame)
        else:
            pos_loops = motifSearch(strSeq, genome.description,args.motif, args.frame)
        str_scores.append(str(genome.description))
        print (str(genome.description) + "\n")
        for pos_loop in pos_loops:
            cur_fragment = pos_loop.sequence
            print ("Nona: " + pos_loop.motif)
            print("Position: " + str(pos_loop.position))
            pos_loop.fold()
            print("Sequence: " + pos_loop.find_stem_loop(args.idealstemlen, args.ideallooplen))
            if args.family is not None: pos_loop.diff_score_modify(bestNona(args.family))
            if pos_loop.stem_start != -1:
                score = pos_loop.score
                if (score < 15 and pos_loop.stem_end > (pos_loop.motif_start + 12) and pos_loop.motif_start > (pos_loop.stem_start + 4)):
                    #features.append(gffutils.Feature(seqid=str(genome.description), source="Stem-Loop_Finder", featuretype="stem_loop", start=pos_loop.stem_start, end=pos_loop.stem_end, strand=".", attributes={'Name': ['stem-loop']}))
                    print("Stem length: " + str(pos_loop.stem_len))
                    print("Loop length: " + str(pos_loop.loop_len))
                    print("Stem-loop start: " + str(pos_loop.stem_start))
                    print("Stem-loop end: " + str(pos_loop.stem_end))
                    features2.append(str(pos_loop.name) + "   Stem-Loop_Finder   stem_loop   " + str(pos_loop.stem_start) + "   " + str(pos_loop.stem_end) + "   .   .   .   Name=stem-loop" + '\n')
                    features2.append(str(pos_loop.name) + "   Stem-Loop_Finder   nonanucleotide   " + str(pos_loop.motif_start) +"   " + str(pos_loop.motif_start + 8) + "   .   .   .   Name=nona" + '\n')
                    str_scores.append(str(score))
            else:
                score = 99
            print (str(score) + " " + cur_fragment + " " + pos_loop.folded + " " + pos_loop.motif)

        print ("\n")
motifs = []
if args.quadrunauto == True:
    for genome in genomes:
        RNA.read_parameter_file(args.params)
        fasta_record = FastaRecord(str(genome.description) + ' sl')
        strSeq = str(genome.sequence) + str(genome.sequence)[0:50]
        if args.family is not None:
            pos_loops = nonaSearch(strSeq, genome.description, args.family, args.frame)
            pos_loops.extend(nonaSearch(strSeq, genome.description, args.family, args.frame2))
            RNA.read_parameter_file(args.params2)
            pos_loops.extend(nonaSearch(strSeq, genome.description, args.family, args.frame))
            pos_loops.extend(nonaSearch(strSeq, genome.description, args.family, args.frame2))
        else:
            pos_loops = motifSearch(strSeq, genome.description,args.motif, args.frame)
            pos_loops = motifSearch(strSeq, genome.description,args.motif, args.frame2)
            RNA.read_parameter_file(args.params2)
            pos_loops = motifSearch(strSeq, genome.description,args.motif, args.frame)
            pos_loops = motifSearch(strSeq, genome.description,args.motif, args.frame2)
        pos_loops = sorted(pos_loops)
        str_scores.append(str(genome.description))
        print (str(genome.description) + "\n")
        pos_loops = removeDuplicates(pos_loops)
        print("REMOVED DUPES")
        last_loop = PosStemLoop("blah", "blah", 15, "blablabla", 20, False)
        for i in range (0, len(pos_loops)):
            pos_loop = pos_loops[i]
            pos_loop.fold()
            print("Sequence: " + pos_loop.find_stem_loop(args.idealstemlen, args.ideallooplen))
            if args.family is not None: pos_loop.diff_score_modify(bestNona(args.family))
            if pos_loop.stem_start != -1:
                score=pos_loop.score
                print("Nona: " + pos_loop.motif)
                print("Nona start: " + str(pos_loop.motif_start))
                print("Position: " + str(pos_loop.position))
                print("Stem length: " + str(pos_loop.stem_len))
                print("Loop length: " + str(pos_loop.loop_len))
                print("Stem-loop start: " + str(pos_loop.stem_start))
                print("Stem-loop end: " + str(pos_loop.stem_end))
                if score < 15 and pos_loop.stem_end > (pos_loop.motif_start + len(motif) + 3) and pos_loop.motif_start > (pos_loop.stem_start + 4):
                    strand = "+"
                    if(pos_loop.reverse_compliment==True):
                        strand = "-"
                    if (i > 0 and pos_loop.stem_start == last_loop.stem_start):
                        if (pos_loop.score - last_loop.score) < 3:
                            if args.family is not None:
                                features2.append(str(pos_loop.name) + "   Stem-Loop_Finder   nonanucleotide   " + str(pos_loop.motif_start) + "   " + str(pos_loop.motif_start + 8) + "   .   " + str(strand) + "   .   Name=nona" + '\n')
                            else:
                                features2.append(str(pos_loop.name) + "   Stem-Loop_Finder   motif   " + str(pos_loop.motif_start) + "   " + str(pos_loop.motif_start + (len(motif) - 1)) + "   .   " + str(strand) + "   .   Name=motif" + '\n')
                            str_scores.append(str(score))
                            motifs.append(pos_loop.name, pos_loop.motif,pos_loop.motif_start,pos_loop.stem_start,pos_loop.stem_end,pos_loop.score,pos_loop.sequence, pos_loop.folded)
                        else:
                            str_scores.append(str(score))
                    else:
                        features2.append(str(pos_loop.name) + "   Stem-Loop_Finder   stem_loop   " + str(pos_loop.stem_start) + "   " + str(pos_loop.stem_end) + "   .   .   .   Name=stem-loop" + '\n')
                        if args.family is not None:
                            features2.append(str(pos_loop.name) + "   Stem-Loop_Finder   nonanucleotide   " + str(pos_loop.motif_start) + "   " + str(pos_loop.motif_start + 8) + "   .   " + str(strand) + "   .   Name=nona" + '\n')
                        else:
                            features2.append(str(pos_loop.name) + "   Stem-Loop_Finder   motif   " + str(pos_loop.motif_start) + "   " + str(pos_loop.motif_start + (len(motif) - 1)) + "   .   " + str(strand) + "   .   Name=motif" + '\n')
                        motifs.append(pos_loop.name, pos_loop.motif,pos_loop.motif_start,pos_loop.stem_start,pos_loop.stem_end,pos_loop.score,pos_loop.sequence, pos_loop.folded)
                        str_scores.append(str(score))
                    last_loop = pos_loop
            else:
                score = 99
            print(str(score) + " " + pos_loop.sequence + " " + pos_loop.folded + " " + pos_loop.motif)
        print("\n")


#db.update(features, ":memory:", id_spec={'gene': 'gene_id', 'transcript': "transcript_id"}, merge_strategy="create_unique", transform=transform_func,keep_order=True)

with open(args.out, 'w') as output_file:
    #for f in db.features_of_type("stem_loop"):
    for f in features2:
        #output_file.write(str(f)[1:]+ '\n')
        output_file.write(str(f)[1:])

if args.CSVout is not None:
    with open(args.CSVout, "w") as output_file:
        for m in motifs:
            output_file.write(m[0]+'\t'+m[1]+'\t'+m[2]+'\t'+m[3]+'\t'+m[4]+'\t'+m[5]+'\t'+m[6]+'\t'+m[7]+"\n")
