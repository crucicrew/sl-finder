#!/bin/bash

# Setting this will cause the script to terminate whenever a subprocess
#  returns an error
# set -e

# Uncomment this on to debug the script
# set -x

shortname="${0##*/}"
longname="StemLoop-Finder"

DOCKER_CMD="docker"

echo "Script is running"

DEF_IMAGE_LOCATION=extremeviruslab/sl_finder
DEF_IMAGE_TAG=latest
DEF_CONTAINER_NAME=sl_finder
DEF_DEBUG=

# export PYTHONPATH=./usr/local/lib/python3.6/site-packages/

function abs_path_for_file()
{
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

function bailout()
{
    local message="$1"
    echo "$shortname: error: ${message}" >&2
    exit 1;
}

function bailout_with_usage()
{
    local message="$1"
    echo "$shortname: error: ${message}" >&2
    print_usage
    exit 1;
}

function print_usage()
{
    echo " "
    echo "Usage: ${shortname} <operation>"
    echo ""
    echo "   operation can be one of:"
    echo ""
    echo "     docker-pull: Download the $shortname docker image"
    echo "     docker-run: Create and start the $shortname docker container"
    echo "     docker-kill: Kill the $shortname docker container"
    echo "     docker-rm: Delete the $shortname docker container (can recreate with docker-run)"
    echo "     docker-update: Kill the container, remove it, update, and restart"
    echo ""
    echo "   [--docker-image <docker image ID>]"
    echo "       (default \"$DEF_IMAGE_LOCATION\")"
    echo "   [--docker-image-tag <docker image tag>]"
    echo "       (default \"$DEF_IMAGE_TAG\")"
    echo "   [--docker-name <docker name to assign>]"
    echo "       (default \"$DEF_CONTAINER_NAME\")"
}

function process_arguments()
{
    shopt -s nullglob
    shopt -s shift_verbose

    operation="docker-run"
    docker_image_id="$DEF_IMAGE_LOCATION"
    docker_image_tag="$DEF_IMAGE_TAG"
    container_name="$DEF_CONTAINER_NAME"
    debug="$DEF_DEBUG"

    fasta_infile=$(abs_path_for_file "$1")
    gff_infile=$(abs_path_for_file "$2")
    outfile=$(abs_path_for_file "$3")
    outfile_dir=$(dirname $outfile)
    outfile_filename=$(basename $outfile)

    shift 3
    while [[ $1 == --* ]]; do
      option_name=$1
      shift
      if [$option_name == "--quadrunauto"]; then
        OPTIONS+=($option_name)
        echo "Found option name $option_name"
      else
        optional_param=$1
        shift
        echo "Found option name $option_name with param $optional_param"
        OPTIONS+=($option_name $optional_param)
        #if [ $option_name == "--CSVout"]; then
        #  bind_csv=true
        #  CSV_file=$optional_param
       fi
    done


    if [ ! -z $debug ]; then
        echo "FASTA input: $fasta_infile"
        echo "GFF input: $gff_infile"
        echo "GFF output: $outfile"
    fi
}

function docker-pull()
{
    echo "Pulling docker image from $docker_image_id:$docker_image_tag"
	$DOCKER_CMD pull $docker_image_id:$docker_image_tag
}

function docker-run()
{
    if [ -z "$fasta_infile" ]; then
        bailout "FASTA input not specified"
    fi

    #if [ -f "$fasta_infile" ]; then
    #    bailout "FASTA input does not exist or is not readable"
    #fi

    if [ -z "$gff_infile" ]; then
        bailout "GFF input not specified"
    fi

    #if [ -f "$gff_infile" ]; then
    #    bailout "GFF input does not exist or is not readable"
    #fi

    if [ -z "$outfile" ]; then
        bailout "Output GFF file not specified"
    fi

    if [ ! -z "$fasta_infile" -a ! -z "$gff_infile" -a ! -z "$outfile" ]; then
        io_mount_args=(--mount type=bind,source="$fasta_infile",target=/infiles/input.fasta,readonly
                       --mount type=bind,source="$gff_infile",target=/infiles/input.gff,readonly
                       --mount type=bind,source="$outfile_dir",target=/outfiles)
        io_command_args=(/infiles/input.fasta /infiles/input.gff /outfiles/$outfile_filename)
    fi


    if [ ! -z "$debug" ]; then
        debug_opt="--debug"
    fi

    docker_run_params=(/usr/bin/python3 stem_loop_search_update.py
                              "${io_command_args[@]}"
                              "${OPTIONS[@]}")



    echo "Starting container \"$container_name\" from $docker_image_id:$docker_image_tag"
    $DOCKER_CMD run --name "$container_name" \
        "${io_mount_args[@]}" \
        "$docker_image_id:$docker_image_tag" \
        "${docker_run_params[@]}"
}


function docker-rm()
{
    docker-kill
    echo "Attempting to remove container \"$container_name\""
    $DOCKER_CMD container rm $container_name
}

function docker-stop()
{
    echo "Attempting to stop container \"$container_name\""
    $DOCKER_CMD container stop $container_name
}

function docker-kill()
{
    echo "Attempting to kill container \"$container_name\""
    $DOCKER_CMD container kill $container_name
}

function docker-restart()
{
    echo "Attempting to restart container \"$container_name\""
    $DOCKER_CMD container restart $container_name
}

function docker-update()
{
    echo "Attempting to update container image \"$container_name\""
    docker-rm
    sleep 1
    docker-pull
    docker-run
}




#
# main logic
#

process_arguments "$@"

$operation

$DOCKER_CMD container rm $container_name
